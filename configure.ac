AC_INIT([auth-sasl-server],[0.1],[info@autistici.org])
AC_CONFIG_SRCDIR([pam.c])
AC_LANG(C)

AM_INIT_AUTOMAKE([foreign])
AC_CONFIG_HEADERS(config.h)
AC_CONFIG_MACRO_DIR([m4])

dnl Program checks.
AC_PROG_CC
AC_PROG_CXX
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_MAKE_SET

CFLAGS="$CFLAGS -Wall -Werror -std=c99 -pedantic"

dnl Checks for libraries.
AX_PTHREAD([have_pthread=yes],[have_pthread=no])
CFLAGS="$CFLAGS $PTHREAD_CFLAGS"
LIBS="$PTHREAD_LIBS $LIBS"

dnl Checks for headers.
AC_HEADER_STDC
AC_HEADER_TIME

dnl Probe for the functionality of the PAM libraries and their include file
dnl naming.  Mac OS X puts them in pam/* instead of security/*.
AC_SEARCH_LIBS([pam_set_data], [pam], [], 
  [AC_MSG_ERROR([libpam not found])])

AC_CHECK_FUNCS([pam_getenv pam_getenvlist pam_modutil_getpwnam])
AC_REPLACE_FUNCS([pam_syslog pam_vsyslog])
AC_CHECK_HEADERS([security/pam_modutil.h], [],
    [AC_CHECK_HEADERS([pam/pam_modutil.h])])
AC_CHECK_HEADERS([security/pam_appl.h], [],
    [AC_CHECK_HEADERS([pam/pam_appl.h], [],
        [AC_MSG_ERROR([No PAM header files found])])])
AC_CHECK_HEADERS([security/pam_ext.h], [],
    [AC_CHECK_HEADERS([pam/pam_ext.h])])
dnl RRA_HEADER_PAM_CONST

dnl Support for libgtest
PKG_CHECK_MODULES([GTEST], [gtest])

dnl Add coverage support to tests
AX_CODE_COVERAGE

dnl Support for systemd socket activation support.
AC_ARG_WITH([systemd], AS_HELP_STRING([--with-systemd], [Build with support for systemd socket activation]))

AS_IF([test "x$with_systemd" = "xyes"], [
    PKG_CHECK_MODULES([SYSTEMD], [libsystemd])
    CFLAGS="$CFLAGS $SYSTEMD_CFLAGS"
    LIBS="$LIBS $SYSTEMD_LIBS"

    AC_DEFINE([HAVE_SYSTEMD], [1], [Enable systemd support (socket activation / notification)])
])

dnl Generate output.
AC_CONFIG_FILES([Makefile])
AC_OUTPUT
