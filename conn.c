#define _GNU_SOURCE
#include "config.h"
#include <errno.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <pthread.h>
#ifdef HAVE_SYSTEMD
#include <systemd/sd-daemon.h>
#endif

#include "base64.h"
#include "pam.h"
#include "conn.h"

// The static cookie for protocol handshake. Not used, never changes.
static const char *static_cookie = "a1a0a32263ab1f9c0cbdf95d1d5dbd9e";

// Global atomic connection counter.
static unsigned long long cuid_counter = 0;
static pthread_mutex_t cuid_counter_mx = PTHREAD_MUTEX_INITIALIZER;

static unsigned long long get_cuid() {
  unsigned long long n;
  pthread_mutex_lock(&cuid_counter_mx);
  n = cuid_counter++;
  pthread_mutex_unlock(&cuid_counter_mx);
  return n;
}

// Create a new connection object, and spawn an associated detached
// thread running the given function.
conn_t *conn_new(int fd, void *(*start_routine)(void *)) {
  conn_t *conn;
  pthread_attr_t attr;
  int err;

  conn = (conn_t *)calloc(1, sizeof(conn_t));
  if (!conn) {
    return NULL;
  }
  conn->cuid = get_cuid();
  conn->fd = fd;

  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
  if ((err = pthread_create(&conn->thread, &attr, start_routine, conn)) != 0) {
    fprintf(stderr, "pthread_create error: %d\n", err);
    free(conn);
    return NULL;
  }
  pthread_attr_destroy(&attr);

  return conn;
}

// Read a line from the given connection.
int conn_read_line(conn_t *conn, char **out_line) {
  int n, avail;
  char *p;

  // Copy what's left in the buffer to the beginning.
  n = conn->buf_tail - conn->buf_head;
  memmove(conn->buf, conn->buf + conn->buf_head, n + 1);
  conn->buf_head = 0;
  conn->buf_tail = n;

  while (1) {
    // Do we have a newline?
    p = strchr(conn->buf, '\n');
    if (p) {
      *p++ = 0;
      *out_line = conn->buf;
      n = (p - conn->buf);
      conn->buf_head = n;
      return n;
    }

    // Read some more data.
    avail = MAX_LINE_LEN - 1 - conn->buf_tail;
    if (avail <= 0) {
      // Line too long.
      return -2;
    }
    n = read(conn->fd, conn->buf + conn->buf_tail, avail);
    if (n <= 0) {
      // Discard incomplete lines at EOF.
      return n;
    }
    conn->buf_tail += n;
    conn->buf[conn->buf_tail] = 0;
  }
}

// Write a line to the connection (printf format and args).
int conn_write(conn_t *conn, const char *format, ...) {
  int n;

  va_list arg_ptr;
  va_start(arg_ptr, format);
  n = vdprintf(conn->fd, format, arg_ptr);
  va_end(arg_ptr);
  return n;
}

// Close connection and free the connection object.
void conn_close(conn_t *conn) {
  close(conn->fd);
  free(conn);
}

#define TAB "\t"

// All the data relevant to an authentication request (though the PAM
// API uses only username, password, and remote IP). Note that, due to
// AUTH / CONT possibly spanning more than one protocol round-trip,
// this object must own all the referenced memory, hence the abundant
// usage of strdup.
//
// The request loop holds a single auth_request in its heap throughout
// its lifetime, so we offer a method to reset it to NULL values
// (freeing previous memory if necessary).
typedef struct auth_request {
  char *id;
  char *mechanism;
  char *service;
  char *rip;
  char *lip;
  char *rport;
  char *lport;
  char *username;
  char *password;
  int secured;
  int has_data;
} auth_request_t;

static auth_request_t *new_auth_request() {
  return (auth_request_t *)calloc(1, sizeof(auth_request_t));
}

static void auth_request_free(auth_request_t *request) {
  if (request->id) {
    free(request->id);
    request->id = NULL;
  }
  if (request->mechanism) {
    free(request->mechanism);
    request->mechanism = NULL;
  }
  if (request->service) {
    free(request->service);
    request->service = NULL;
  }
  if (request->rip) {
    free(request->rip);
    request->rip = NULL;
  }
  if (request->lip) {
    free(request->lip);
    request->lip = NULL;
  }
  if (request->rport) {
    free(request->rport);
    request->rport = NULL;
  }
  if (request->lport) {
    free(request->lport);
    request->lport = NULL;
  }
  if (request->username) {
    free(request->username);
    request->username = NULL;
  }
  if (request->password) {
    free(request->password);
    request->password = NULL;
  }
  free(request);
}

static int parse_plain_auth_data(char *data, char **username, char **password) {
  size_t i, j, state, n, slen;
  unsigned char *buf;

  slen = strlen(data);
  n = slen;
  buf = (unsigned char *)malloc(n + 1);
  if (base64_decode(buf, &n, (const unsigned char *)data, slen) < 0) {
    goto fail;
  }
  if (n < 2) {
    goto fail;
  }
  buf[n] = '\0';

  // Split the buffer into three NUL-separated fields
  // ([authzid] / authcid / passwd).
  for (i = 0, j = 0, state = 0; i <= n; i++) {
    if (buf[i] == 0) {
      switch (state++) {
      case 1:
        *username = strdup((char *)(buf + j));
        break;
      case 2:
        *password = strdup((char *)(buf + j));
        break;
      }
      j = i + 1;
    }
  }

  if (state != 3) {
    // Wrong number of fields.
    goto fail;
  }

  free(buf);
  return 0;

 fail:
  free(buf);
  return -1;
}

static char *get_next_token_dup(char **saveptr) {
  char *token = strtok_r(NULL, TAB, saveptr);
  if (!token) {
    return NULL;
  }
  return strdup(token);
}

#define MAX_PENDING_REQUESTS 64

// To manage pending requests, we just keep a list of them and scan it
// sequentially (comparing request IDs). Slots are freed when a
// request is popped from the buffer.
typedef auth_request_t **pending_request_buf_t;

static int pending_requests_push(pending_request_buf_t buf, auth_request_t *request) {
  for (int i = 0; i < MAX_PENDING_REQUESTS; i++) {
    if (buf[i] == NULL) {
      buf[i] = request;
      return 0;
    }
  }
  return -1;
}

static auth_request_t *pending_requests_pop(pending_request_buf_t buf, const char *request_id) {
  for (int i = 0; i < MAX_PENDING_REQUESTS; i++) {
    if (buf[i] != NULL && !strcmp(buf[i]->id, request_id)) {
      auth_request_t *request = buf[i];
      buf[i] = NULL;
      return request;
    }
  }
  return NULL;
}

static void pending_requests_free(pending_request_buf_t buf) {
  for (int i = 0; i < MAX_PENDING_REQUESTS; i++) {
    if (buf[i] != NULL) {
      auth_request_free(buf[i]);
    }
  }
}

static int parse_auth_request(char **saveptr, auth_request_t *request) {
  // The first two arguments (id and mechanism) are fixed.
  request->id = get_next_token_dup(saveptr);
  if (!request->id) {
    return -1;
  }
  request->mechanism = get_next_token_dup(saveptr);
  if (!request->mechanism) {
    return -1;
  }

  // The remaining tokens are attributes, either key=value or
  // booleans.
  while (1) {
    char *token = strtok_r(NULL, TAB, saveptr), *p;
    if (!token) {
      break;
    }

    if (!strcmp(token, "secured")) {
      request->secured = 1;
    } else if ((p = strchr(token, '=')) != NULL) {
      *p++ = '\0';
      if (!strcmp(token, "rip")) {
        request->rip = strdup(p);
      } else if (!strcmp(token, "rport")) {
        request->rport = strdup(p);
      } else if (!strcmp(token, "lip")) {
        request->lip = strdup(p);
      } else if (!strcmp(token, "lport")) {
        request->lport = strdup(p);
      } else if (!strcmp(token, "service")) {
        request->service = strdup(p);
      } else if (!strcmp(token, "resp")) {
        // We should be ignoring tabs on the rest of the line but we
        // aren't, resulting in a stricter implementation of the spec.
        if (parse_plain_auth_data(p, &request->username, &request->password) < 0) {
          fprintf(stderr, "failed to parse plain auth data: %s\n", p);
          return -1;
        }
        request->has_data = 1;
      } else {
        // Ignore other tokens.
      }
    } else {
      // Unsupported boolean value.
    }
  }

  return 0;
}

static int parse_cont_request(char **saveptr, pending_request_buf_t buf, auth_request_t **out_request) {
  auth_request_t *request;
  char *id, *data;

  id = strtok_r(NULL, TAB, saveptr);
  if (!id) {
    return -1;
  }

  request = pending_requests_pop(buf, id);
  if (request == NULL) {
    fprintf(stderr, "error: CONT: unknown request id '%s'\n", id);
    return -1;
  }
  *out_request = request;

  data = strtok_r(NULL, TAB, saveptr);
  if (!data) {
    fprintf(stderr, "error: CONT command with no data\n");
    return -1;
  }

  // If parse_plain_auth_data succeeds, we have a valid request
  // at this point.
  if (parse_plain_auth_data(data, &request->username, &request->password) < 0) {
    fprintf(stderr, "error: bad base64 data in CONT command\n");
    return -1;
  }
  request->has_data = 1;
  return 0;
}

static int authenticate_request(auth_context_t *auth_ctx, auth_request_t *request) {
  int ret = -1;
  const char *auth_error = NULL;
  
  if (strcmp(request->mechanism, "PLAIN") == 0) {
    if (authenticate(auth_ctx, request->username, request->password, request->rip, &auth_error) == 0) {
      // Success.
      ret = 0;
    }
  }

  fprintf(stderr,
          "auth request: mech=%s service=%s rip=%s%s%s lip=%s%s%s user=%s status=%s%s\n",
          request->mechanism,
          request->service ? request->service : "",
          request->rip ? request->rip : "",
          request->rport ? ":" : "",
          request->rport ? request->rport : "",
          request->lip ? request->lip : "",
          request->lport ? ":" : "",
          request->lport ? request->lport : "",
          request->username,
          (ret == 0) ? "ok" : "error - ",
          (ret == 0) ? "" : auth_error);
  return ret;
}

// This part is the authentication handler common to AUTH and CONT commands.
static int handle_authentication(conn_t *conn, auth_context_t *auth_ctx, auth_request_t *request) {
  // Perform the authentication.
  if (authenticate_request(auth_ctx, request) < 0) {
    return conn_write(conn, "FAIL\t%s\tuser=%s\n", request->id, request->username);
  } else {
    return conn_write(conn, "OK\t%s\tuser=%s\n", request->id, request->username);
  }
}

#define CONN_STATE_WAITING_FOR_HANDSHAKE 0
#define CONN_STATE_WAITING_FOR_CPID 1
#define CONN_STATE_AUTH 2
#define CONN_STATE_CONT 3       // unused

static void *handle_connection(void *arg) {
  conn_t *conn = (conn_t *)arg;
  char *line;
  auth_request_t *pending_requests[MAX_PENDING_REQUESTS] = {0};
  int state = CONN_STATE_WAITING_FOR_HANDSHAKE;
  auth_context_t *auth_ctx;
  auth_request_t *request;

  // Create the authentication context for this thread.
  auth_ctx = auth_context_init(pam_service);
  if (!auth_ctx) {
    fprintf(stderr, "error: could not create auth_context\n");
    return (void *)-1;
  }

  // Send our handshake. Postfix really cares about ordering here, it
  // needs to see VERSION / MECH / SPID / CUID in order to figure out
  // it is talking to a Dovecot auth-client socket.
  if (conn_write(conn,
                 "VERSION\t1\t1\n"
                 "MECH\tPLAIN\tplaintext\n"
                 "SPID\t%d\n"
                 "CUID\t%lld\n"
                 "COOKIE\t%s\n"
                 "DONE\n",
                 getpid(),
                 conn->cuid,
                 static_cookie) < 0) {
    goto fail;
  }

  // The connection handler is just a simple state machine (to take
  // care of the initial handshake).
  while (conn_read_line(conn, &line) > 0) {
    char *cmd, *saveptr;

    // Tokenize the string to separate the command from its
    // arguments. When parsing the line, remember that the parsed
    // tokens still point at the line buffer, and should not be scoped
    // outside of this inner loop.
    saveptr = NULL;
    cmd = strtok_r(line, TAB, &saveptr);
    if (!cmd) {
      fprintf(stderr, "malformed input\n");
      goto fail;
    }

    switch(state) {
    case CONN_STATE_WAITING_FOR_HANDSHAKE:
      if (!strcmp(cmd, "VERSION")) {
        char *major_version = strtok_r(NULL, TAB, &saveptr);
        if (!major_version) {
          fprintf(stderr, "error: malformed VERSION line\n");
          goto fail;
        }
        if (strcmp(major_version, "1") != 0) {
          fprintf(stderr, "error: incompatible major version %s\n", major_version);
          goto fail;
        }
        state = CONN_STATE_WAITING_FOR_CPID;
      } else {
        fprintf(stderr, "unexpected command '%s' during handshake (expected VERSION)\n", cmd);
        goto fail;
      }
      break;

    case CONN_STATE_WAITING_FOR_CPID:
      if (!strcmp(cmd, "CPID")) {
        char *cpid = strtok_r(NULL, "\t", &saveptr);
        if (!cpid) {
          fprintf(stderr, "error: malformed CPID line\n");
          goto fail;
        }
        state = CONN_STATE_AUTH;
      } else {
        fprintf(stderr, "error: unexpected command '%s' during handshake\n", cmd);
        goto fail;
      }
      break;

    case CONN_STATE_AUTH:
      // Due to pipelining, in the AUTH state we can accept both AUTH
      // and CONT commands.
      if (!strcmp(cmd, "AUTH")) {
        // Create a new request object. The code path that requires a
        // CONT request must not free this object.
        request = new_auth_request();

        if (parse_auth_request(&saveptr, request) < 0) {
          fprintf(stderr, "error: malformed AUTH request\n");
          free(request);
          goto fail;
        }

        // If the request is not complete, send a CONT. Otherwise,
        // authenticate.
        if (!request->has_data) {
          if (pending_requests_push(pending_requests, request) < 0) {
            fprintf(stderr, "error: too many pending requests\n");
            auth_request_free(request);
            goto fail;
          }
          if (conn_write(conn, "CONT\t%s\t\n", request->id) < 0) {
            fprintf(stderr, "error writing response: %s\n", strerror(errno));
            pending_requests_pop(pending_requests, request->id);
            auth_request_free(request);
            goto fail;
          }
        } else {
          if (handle_authentication(conn, auth_ctx, request) < 0) {
            fprintf(stderr, "error writing response: %s\n", strerror(errno));
            auth_request_free(request);
            goto fail;
          }
          auth_request_free(request);
        }

      } else if (!strcmp(cmd, "CONT")) {
        // Set to NULL so we can free it in case parse_cont_request()
        // fails after retrieving a valid request from the pending
        // buffer.
        request = NULL;
        if (parse_cont_request(&saveptr, pending_requests, &request) < 0) {
          if (request != NULL) {
            auth_request_free(request);
          }
          goto fail;
        }

        handle_authentication(conn, auth_ctx, request);
        auth_request_free(request);

      } else {
        fprintf(stderr, "error: unexpected command '%s'\n", cmd);
        goto fail;
      }
      break;

    default:
      fprintf(stderr, "panic: unexpected connection state %d\n", state);
      goto fail;
    }
  }

 fail:
  conn_close(conn);
  pending_requests_free(pending_requests);
  auth_context_free(auth_ctx);
  return 0;
}

// Accept incoming connections and handle them.
//
// We're using a simple one-thread-per-connection model, because the
// actual work in of handling the authentication request is done by
// the PAM library, and we can't make it asynchronous. Threads are
// fire-and-forget, there's no graceful shutdown, except for a
// one-second delay after having closed the socket.
int accept_loop(int sock) {
  int ret = -1;

#ifdef HAVE_SYSTEMD
  // Let systemd know that we're ready.
  sd_notify(0, "READY=1");
#endif

  signal(SIGPIPE, SIG_IGN);
  signal(SIGTERM, exit);

  while (1) {
    int conn_fd;
    conn_t *conn;

    if ((conn_fd = accept(sock, NULL, NULL)) < 0) {
      if (errno == EAGAIN || errno == EINTR || errno == ECONNABORTED) {
        continue;
      }
      if (errno == EBADF) {
        // All ok, the socket has been closed, clean shutdown.
        ret = 0;
        fprintf(stderr, "no longer accepting connections\n");
      } else {
        fprintf(stderr, "socket error: %s\n", strerror(errno));
      }

      // Wait a small amount of time and then interrupt all pending
      // connections.
#ifdef HAVE_SYSTEMD
      sd_notify(0, "STOPPING=1");
#endif
      sleep(1);

      return ret;
    }

    // Spawn off a new thread to handle the connection.
    conn = conn_new(conn_fd, handle_connection);
    if (!conn) {
      fprintf(stderr, "can't handle connection\n");
      close(conn_fd);
      continue;
    }
  }
}

int create_socket(const char *socket_path, int *out_sock) {
  struct sockaddr_un addr;
  int sock_fd;

#ifdef HAVE_SYSTEMD
  int n = sd_listen_fds(0);
    
  if (n > 1) {
    fprintf(stderr, "Too many file descriptors received!\n");
    return -1;
  }
  if (n == 1) {
    sock_fd = SD_LISTEN_FDS_START + 0;
  } else {
#endif

    if (!socket_path) {
      fprintf(stderr, "Must specify --socket\n");
      return -1;
    }

    sock_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sock_fd < 0) {
      fprintf(stderr, "error creating socket: %s\n", strerror(errno));
      return -1;
    }

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path)-1);

    // Unlink the socket before binding.
    unlink(socket_path);
  
    if (bind(sock_fd, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
      fprintf(stderr, "socket error: bind(): %s\n", strerror(errno));
      return -1;
    }

    if (listen(sock_fd, 5) < 0) {
      fprintf(stderr, "socket error: listen(): %s\n", strerror(errno));
      return -1;
    }

#ifdef HAVE_SYSTEMD
  }
#endif

  *out_sock = sock_fd;
  return 0;
}
