#ifndef __base64_H
#define __base64_H 1

#include <sys/types.h>

size_t base64_encode_size(size_t);
int base64_encode(unsigned char *, size_t *, const unsigned char *, size_t);
int base64_decode(unsigned char *, size_t *, const unsigned char *, size_t);

#define ERR_BUFFER_TOO_SMALL -1
#define ERR_DECODE64 -2

#endif
