#define _GNU_SOURCE
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#if defined(HAVE_SECURITY_PAM_APPL_H)
#include <security/pam_appl.h>
#elif defined(HAVE_PAM_PAM_APPL_H)
#include <pam/pam_appl.h>
#endif

#include "pam.h"

const char *pam_service = "smtp";

static int conv_get_password(int num_msg,
                             const struct pam_message __attribute__((unused)) **msg,
                             struct pam_response **resp,
                             void *appdata_ptr) {
  char *password = (char *)appdata_ptr;
  int i;

  *resp = calloc(num_msg, sizeof(struct pam_response));
  if (!*resp) {
    return PAM_BUF_ERR;
  }
  for (i = 0; i < num_msg; i++) {
    (*resp)[i].resp = strdup(password);
  }

  return PAM_SUCCESS;
}

// This is only passed to pam_start, we override the conversation
// callback on every call to authenticate().
static struct pam_conv conv_start = {conv_get_password, 0};

auth_context_t *auth_context_init(const char *pam_service_name) {
  auth_context_t *ctx = (auth_context_t *)calloc(1, sizeof(auth_context_t));
  if (pam_start(pam_service_name, NULL, &conv_start, &ctx->pamh) != PAM_SUCCESS) {
    free(ctx);
    return NULL;
  }
  return ctx;
}

void auth_context_free(auth_context_t *ctx) {
  pam_end(ctx->pamh, PAM_SUCCESS|PAM_SILENT);
  free(ctx);
}

int authenticate(auth_context_t *ctx, const char *username, const char *password, const char *rhost, const char **errmsg) {
  int err;
  struct pam_conv conv = {
                          conv_get_password,
                          (void *)password,
  };
  if ((err = pam_set_item(ctx->pamh, PAM_USER, username)) != PAM_SUCCESS) {
    *errmsg = pam_strerror(ctx->pamh, err);
    return -1;
  }
  if ((err = pam_set_item(ctx->pamh, PAM_CONV, &conv)) != PAM_SUCCESS) {
    *errmsg = pam_strerror(ctx->pamh, err);
    return -1;
  }
  if (rhost != NULL && (err = pam_set_item(ctx->pamh, PAM_RHOST, rhost)) != PAM_SUCCESS) {
    *errmsg = pam_strerror(ctx->pamh, err);
    return -1;
  }
  if ((err = pam_authenticate(ctx->pamh, PAM_DISALLOW_NULL_AUTHTOK)) != PAM_SUCCESS) {
    *errmsg = pam_strerror(ctx->pamh, err);
    return -1;
  }
  return 0;
}

