#ifndef __pam_H
#define __pam_H 1

#include "config.h"

#if defined(HAVE_SECURITY_PAM_APPL_H)
#include <security/pam_appl.h>
#elif defined(HAVE_PAM_PAM_APPL_H)
#include <pam/pam_appl.h>
#endif

typedef struct auth_context {
  pam_handle_t *pamh;
} auth_context_t;

extern const char *pam_service;

auth_context_t *auth_context_init();
void auth_context_free(auth_context_t *);
int authenticate(auth_context_t *, const char *, const char *, const char *, const char **);

#endif
