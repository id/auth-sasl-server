#ifndef __conn_H
#define __conn_H 1

#include <pthread.h>

#define MAX_LINE_LEN 8192

// Connection object. Includes a line read buffer.
typedef struct conn {
  unsigned long long cuid;
  int fd;
  pthread_t thread;

  char buf[MAX_LINE_LEN];
  int buf_head, buf_tail;

  struct conn *prev, *next;
} conn_t;

conn_t *conn_new(int, void *(*)(void *));
int conn_read_line(conn_t *, char **);
int conn_write(conn_t *, const char *, ...);
void conn_close(conn_t *);

int accept_loop(int);
int create_socket(const char *, int *);

#endif
