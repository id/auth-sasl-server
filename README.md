auth-sasl-server
===

A connector for Postfix SASL smtpd authentication to the
[auth-server](https://git.autistici.org/id/auth), using the [Dovecot
SASL protocol](https://dovecot.org/doc/auth-protocol.txt). You can
also integrate it with other authentication mechanisms as it simply
uses PAM for authentication.

It is already possible to connect Postfix, for SMTP authentication, to
an auth-server process, using either Dovecot itself, or Cyrus
saslauthd with the PAM
[authclient](https://git.autistici.org/id/auth-pam) module. However
there are cases where neither solution is desirable: configuring a
standalone Dovecot instance just for authentication isn't trivial, and
there's a long-standing bug in saslauthd
([1](https://github.com/cyrusimap/cyrus-sasl/issues/346),
[2](https://github.com/cyrusimap/cyrus-sasl/pull/6)), where client's
IP address is not passed along to the PAM module, which means that the
auth-server can't apply any IP-based rate-limits or
blacklists. Another problem with saslauthd is that is has a low limit
for username/password size (256), which causes issues with very long
usernames and our SSO schema. As this is a pretty severe limitation,
we decided to write this little daemon as a workaround.

This daemon uses Dovecot's authentication protocol because it is
simpler to implement than whatever libsasl is doing, and because
Postfix can already talk to it (using *smtpd_sasl_type=dovecot*).

# Installation

It's a standard autoconf-based project, so:

```shell
$ sh autogen.sh
$ ./configure
$ make
$ sudo make install
```

# Usage

Just start the *auth-sasl-server* daemon with the *--socket* option
(mandatory) to point it at the UNIX socket that it should listen
on. The daemon supports the systemd socket activation protocol, in
which case the *--socket* option is not necessary.

Note that while we're calling it a "daemon", the *auth-sasl-server*
program does not fork into the background or switch user ID, it is
expected that the caller will take care of setting up the environment
for secure operation.

Set the PAM service to use by specifying the *--pam-service*
command-line option. The default is *smtp*.
