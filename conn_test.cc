#include <gtest/gtest.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/wait.h>

extern "C" {
#include "conn.h"
}

#define SOCKET_PATH ".test-sock"

class SASLConnTestSuite : public testing::Test
{
  void SetUp() {
    unlink(SOCKET_PATH);
    if (create_socket(SOCKET_PATH, &sockfd) < 0) {
      throw "error";
    }

    pid = fork();
    if (pid == 0) {
      accept_loop(sockfd);
    } else if (pid < 0) {
      throw "fork()";
    }
  }
 
  void TearDown() {
    close(sockfd);
    if (pid > 0) {
      kill(pid, 15);
      waitpid(pid, NULL, 0);
    }
    unlink(SOCKET_PATH);
  }
  
  int sockfd;
  pid_t pid;

public:
  // Create a connection, re-using a conn_t just for its buffering so
  // we can call conn_read_line() and conn_write().
  conn_t *mkconn() {
    int fd;
    struct sockaddr_un addr;

    fd = socket(AF_UNIX, SOCK_STREAM, 0);
    EXPECT_GT(fd, 0);

    addr.sun_family = AF_UNIX;
    strcpy(addr.sun_path, SOCKET_PATH);
    EXPECT_GE(connect(fd, (const struct sockaddr *)&addr, sizeof(sockaddr_un)), 0);

    conn_t *conn = (conn_t *)calloc(1, sizeof(conn_t));
    conn->fd = fd;
    return conn;
  }

  // Perform the initial client-side handshake (without looking at the
  // received values too closely though).
  void handshake(conn_t *conn) {
    const char *line = "VERSION\t1\t1\nCPID\t1234\n";
    ASSERT_GE(conn_write(conn, line), 0);
    int i;
    for (i = 0; i < 10; i++) {
      char *p;
      ASSERT_GE(conn_read_line(conn, &p), 0);
      if (!strcmp(p, "DONE")) {
        break;
      }
    }
    ASSERT_LT(i, 10);
  }

  // Exchange requests/responses. Additional arguments are the
  // (possibly more than one) expected responses.
  void exchange(conn_t *conn, const char *request, ...) {
    ASSERT_GE(conn_write(conn, request), 0);

    char *p, *arg;
    std::string out;
    va_list arg_ptr;
    va_start(arg_ptr, request);
    while ((arg = va_arg(arg_ptr, char *)) != NULL) {
      ASSERT_GT(conn_read_line(conn, &p), 0);
      std::string out = p;
      out += "\n";
      EXPECT_EQ(out, std::string(arg));
    }
  }
};

TEST_F(SASLConnTestSuite, single_auth_request) {
  conn_t *conn = mkconn();
  
  handshake(conn);
  exchange(conn,
           "AUTH\t8\tPLAIN\tservice=smtp\tnologin\tlip=198.167.222.108\trip=1.2.3.4\tsecured\tresp=YXV0aHppZABhdXRoY2lkAHBhc3N3b3Jk\n",
           "FAIL\t8\tuser=authcid\n",
           NULL);

  conn_close(conn);
}

TEST_F(SASLConnTestSuite, auth_cont_request) {
  conn_t *conn = mkconn();

  handshake(conn);
  exchange(conn,
           "AUTH\t8\tPLAIN\tservice=smtp\tnologin\tlip=198.167.222.108\trip=1.2.3.4\tsecured\n",
           "CONT\t8\t\n",
           NULL);
  exchange(conn,
           "CONT\t8\tYXV0aHppZABhdXRoY2lkAHBhc3N3b3Jk\n",
           "FAIL\t8\tuser=authcid\n",
           NULL);

  conn_close(conn);
}

TEST_F(SASLConnTestSuite, pipelined_auth_requests) {
  conn_t *conn = mkconn();

  handshake(conn);
  exchange(conn,
           "AUTH\t8\tPLAIN\tservice=smtp\tnologin\tlip=198.167.222.108\trip=1.2.3.4\tsecured\n"
           "AUTH\t9\tPLAIN\tservice=smtp\tnologin\tlip=198.167.222.108\trip=1.2.3.4\tsecured\n"
           "AUTH\t10\tPLAIN\tservice=smtp\tnologin\tlip=198.167.222.108\trip=1.2.3.4\tsecured\n",
           "CONT\t8\t\n",
           "CONT\t9\t\n",
           "CONT\t10\t\n",
           NULL);

  // Authenticate the ids in non-sequential order.
  int ids[] = {9, 8, 10};
  for (int i = 0; i < 3; i++) {
    char request[128], response[128];
    sprintf(request, "CONT\t%d\tYXV0aHppZABhdXRoY2lkAHBhc3N3b3Jk\n", ids[i]);
    sprintf(response, "FAIL\t%d\tuser=authcid\n", ids[i]);
    exchange(conn, request, response, NULL);
  }

  conn_close(conn);
}

TEST_F(SASLConnTestSuite, pipelined_auth_requests_incomplete) {
  conn_t *conn = mkconn();

  handshake(conn);
  exchange(conn,
           "AUTH\t8\tPLAIN\tservice=smtp\tnologin\tlip=198.167.222.108\trip=1.2.3.4\tsecured\n"
           "AUTH\t9\tPLAIN\tservice=smtp\tnologin\tlip=198.167.222.108\trip=1.2.3.4\tsecured\n"
           "AUTH\t10\tPLAIN\tservice=smtp\tnologin\tlip=198.167.222.108\trip=1.2.3.4\tsecured\n",
           "CONT\t8\t\n",
           "CONT\t9\t\n",
           "CONT\t10\t\n",
           NULL);

  // By not completing the CONT requests, we can verify using a
  // sanitizer that the request context does not leak the memory of
  // the pending request upon close.
  conn_close(conn);
}

TEST_F(SASLConnTestSuite, bad_cont_request) {
  conn_t *conn = mkconn();
  char *p;

  handshake(conn);
  ASSERT_GE(conn_write(conn, "CONT\t8\t\n"), 0);
  ASSERT_EQ(conn_read_line(conn, &p), 0);
  conn_close(conn);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
