#define _GNU_SOURCE
#include "config.h"
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <signal.h>

#include "conn.h"
#include "pam.h"

static struct option long_options[] = {
                                       {"help", no_argument, 0, 'h'},
                                       {"pam-service", required_argument, 0, 'P'},
                                       {"socket", required_argument, 0, 's'},
                                       {0, 0, 0, 0}
};

static int sock_fd = 0;

static void sig_handler(int signum) {
  fprintf(stderr, "terminating on signal %d\n", signum);
  close(sock_fd);
}

static int usage() {
  fprintf(stderr,
          "%s\n"
          "Usage: auth-sasl-server [<options>]\n"
          "Known options:\n"
          "\n"
          "  -h, --help                  Show this help message\n"
          "  -s PATH, --socket=PATH      Location of the UNIX socket\n"
          "  -P NAME, --pam-service=NAME\n"
          "                              Service name for PAM (default: '%s')\n"
          "\n\n",
          PACKAGE_STRING,
          pam_service);
  return 2;
}

int main(int argc, char **argv) {
  char *socket_path = NULL;

  while (1) {
    int c, option_index = 0;
    if ((c = getopt_long(argc, argv, "hP:s:", long_options, &option_index)) < 0) {
      break;
    }
    switch (c) {
    case 'h':
      return usage();
    case 'P':
      pam_service = optarg;
      break;
    case 's':
      socket_path = optarg;
      break;
    }
  }

  if (optind < argc) {
    fprintf(stderr, "Too many arguments\n\n");
    return usage();
  }

  if (create_socket(socket_path, &sock_fd) < 0) {
    return 1;
  }

  signal(SIGPIPE, SIG_IGN);
  signal(SIGINT, sig_handler);
  signal(SIGTERM, sig_handler);
  fprintf(stderr, "server starting\n");

  if (accept_loop(sock_fd) < 0) {
    return 1;
  }
  fprintf(stderr, "server stopped\n");
  return 0;
}
